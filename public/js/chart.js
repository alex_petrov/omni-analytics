google.setOnLoadCallback(drawChart);

function drawChart() {
    if ($('#day-pills').hasClass('active')) {
        drawDayChart();
    }
    if ($('#compare-pills').hasClass('active')) {
        drawCompareChart();
    }
    if ($('#average-pills').hasClass('active')) {
        drawAverageChart();
    }
}

function drawDayChart() {
    $.getJSON('/ajax/growth?domain=' + group + "&date=" + $('#day-pills #date-picker').val(), function (result) {
        if ($('#day-pills #type-picker').val() == "relative") {
            for (i = result.length - 1; i > 1; i--) {
                result[i][1] = result[i][1] - result[i - 1][1];
                if (i == 2) {
                    result[1][1] = 0;
                }
            }
        }
        if (result.length > 1) {
            var data = google.visualization.arrayToDataTable(result);
            var options = {
                title: 'Изменение аудитории',
                curveType: 'function',
                legend: {
                    position: 'bottom'
                }
            };

            var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
            chart.draw(data, options)
            $('.no-data').hide();
        }
        else {
            $('.no-data').show();
        }
    });
}

function drawCompareChart() {
    $.getJSON('/ajax/growth-compare?domain=' + group + "&date1=" + $('#compare-pills #date-picker-1').val() + "&date2=" + $('#compare-pills #date-picker-2').val(), function (result) {
        for (i = result.length - 1; i > 1; i--) {
            result[i][1] = result[i][1] - result[i - 1][1];
            result[i][2] = result[i][2] - result[i - 1][2];
            if (i == 2) {
                result[1][1] = 0;
                result[1][2] = 0;
            }
        }
        if (result.length > 1) {
            var data = google.visualization.arrayToDataTable(result);
            var options = {
                title: 'Изменение аудитории',
                curveType: 'function',
                legend: {
                    position: 'bottom'
                }
            };

            var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
            chart.draw(data, options)
            $('.no-data').hide();
        }
        else {
            $('.no-data').show();
        }
    });
}

function drawAverageChart() {
    $.getJSON('/ajax/growth-average?domain=' + group + "&date1=" + $('#average-pills #date-picker-begin').val() + "&date2=" + $('#average-pills #date-picker-end').val(), function (result) {
        debugger;
        for (i = result.length - 1; i > 1; i--) {
            sum = 0;
            for(j = 0; j < result[i][1].length; j++) {
                sum += result[i][1][j] - result[i - 1][1][j];
            }
            result[i][1] = (sum / result[i][1].length).toFixed(2);
            if (i == 2) {
                result[1][1] = 0;
            }
        }
        
        if (result.length > 1) {
            var data = google.visualization.arrayToDataTable(result);
            var options = {
                title: 'Изменение аудитории',
                curveType: 'function',
                legend: {
                    position: 'bottom'
                }
            };

            var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
            chart.draw(data, options)
            $('.no-data').hide();
        }
        else {
            $('.no-data').show();
        }
    });
}

$(document).ready(function () {
    $('#group-picker').on('change', function () {
        drawChart();
    })
    $('#type-picker').on('change', function () {
        drawChart();
    });
    $(".datepicker").datepicker({format: "yyyy-mm-dd"}).on('changeDate', function (ev) {
        drawChart();
    });
    $('.nav-pills li').on('click', function () {
        drawChart();
    })
});