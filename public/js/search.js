$(document).ready(function () {
    json.sort(sort_reposts);
    load(30);
    $(window).scroll(function () {
        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            load(30);
        }
    });
    $("input[name='sort']").change(function () {
        selected = $(this).val();
        if (selected == "likes") {
            json.sort(sort_likes);
        }
        if (selected == "reposts") {
            json.sort(sort_reposts);
        }
        if (selected == "comments") {
            json.sort(sort_comments);
        }
        if (selected == "er") {
            json.sort(sort_er);
        }
        $('#search-results').html('');
        currentPosition = 0;
        load(30);
    });
});

function sort_likes(a, b) {
    if (a.likes < b.likes)
        return 1;
    if (a.likes > b.likes)
        return -1;
    return 0;
}

function sort_reposts(a, b) {
    if (a.reposts < b.reposts)
        return 1;
    if (a.reposts > b.reposts)
        return -1;
    return 0;
}

function sort_comments(a, b) {
    if (a.comments < b.comments)
        return 1;
    if (a.comments > b.comments)
        return -1;
    return 0;
}

function sort_er(a, b) {
    if (a.er < b.er)
        return 1;
    if (a.er > b.er)
        return -1;
    return 0;
}

function get_frame(post) {
    debugger;
    if (post !== undefined) {
        embed = "<div class=\"col-lg-9\" style=\"align:center\">";
        embed = embed + "<div class=\"panel panel-default\">";
        embed = embed + "<div class=\"panel-heading\">";
        embed = embed + "Репостов: " + post.reposts + " Лайков: " + post.likes + " Комментариев: " + post.comments + " ER: " + post.er.toFixed(2) + "%";
        embed = embed + "</div>";
        embed = embed +"<div class=\"panel-body\">";
        embed = embed + "<p>" + post.text + "</p>";
        ebmed = embed + "<div class=\"wall\">";
        for (i = 0; i < post.attachments.length; i++) {
            embed = embed + "<img src=\"" + post.attachments[i] + "\">";
        }
        embed = embed + "</div>";
        embed = embed + "<div class=\"panel-footer\">";
        embed = embed + "<a target=\"_blank\" href=\"http://vk.com/wall" + post.url + "\">http://vk.com/wall" + post.url + "</a>";
        embed = embed + "</div>";        
        embed = embed + "</div>";
        embed = embed + "</div>";
    }
    else {
        embed = '';
    }
    return embed;
}

function load($count) {
    ourI = currentPosition;
    while(ourI < (currentPosition + $count)) {
        $('#search-results').html($('#search-results').html() + get_frame(json[ourI]));
        ourI += 1;
    }
    currentPosition = ourI;       
    stylize();
}

function stylize() {
    $('.wall').collagePlus();
    $('#search-results p').readmore();     
}