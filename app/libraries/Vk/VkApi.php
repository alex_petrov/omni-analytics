<?php
/**
 * Created by PhpStorm.
 * User: alexpetrov
 * Date: 1/29/15
 * Time: 10:56 PM
 */

namespace Vk;


class VkApi {

  protected static $_instance;
  private function __construct(){
  }

  private function __clone(){
  }

  public static function getInstance() {
    if (null === self::$_instance) {
      self::$_instance = new self();
    }
    return self::$_instance;
  }

  public static function api($method, $name) {

  }

  public function sendPost($post_url, $post_data, $refer)
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $post_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_REFERER, $refer);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17');

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
  }

} 