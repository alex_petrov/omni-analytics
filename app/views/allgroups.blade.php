@extends('layouts.main')
@section('head')
@parent
@stop
@section('body')
<div id="page-wrapper">
    <div class="row">        
        <div class="col-lg-12">
            <h1 class="page-header">Анализ роста групп</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <?php if(isset($_GET['error'])):?>
                <?php if($_GET['error'] == "already"): ?>
                    <div class="alert alert-info">
                        Группа уже добавлена
                    </div>            
                <?php else: ?>
                    <div class="alert alert-danger">
                        Произошла ошибка. Попробуйте еще раз либо обратитесь в поддержку.
                    </div>            
                <?php endif; ?>
            <?php endif; ?>
            <div class="alert alert-success no-data" style="display:none">
                Анализируемые группы
            </div>            
            <div class="panel panel-default">
                <div class="panel-heading"  style="height:56px;">
                    <form method="get" action="/growth/add" class="">
                    <span class="col-md-6">
                        <div class="form-group input-group">
                            <span class="input-group-addon">URL</span>                        
                            <input name="url" type="text" class="form-control">
                        </div>
                    </span>
                    <span class="col-md-6">
                        <button type="submit" class="btn btn-default">Добавить</button>                        
                    </span>
                    </form>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                        <div class="panel-heading">
                            Группы
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>№</th>
                                            <th>Название</th>
                                            <th>Действия</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $i = 0;
                                        foreach($groups as $group): 
                                            $i++;
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $group->name; ?> (<a href="<?php echo $group->url(); ?>"><?php echo $group->url(); ?></a>)</td>
                                            <td>
                                                <button onclick="location.href='/growth/statistics?group=<?php echo $group->domain; ?>';" type="button" class="btn btn-default">Анализ</button>
                                                <?php if(User::current()->id == "3559971"): ?>
                                                <button onclick="location.href='/growth/delete/<?php echo $group->domain; ?>';" type="button" class="btn btn-danger">Удалить</button>
                                                <?php endif;?>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>               
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

</div>
<!-- /#page-wrapper -->
@stop
@section('footer')
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="/bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="/dist/js/sb-admin-2.js"></script>
@stop