<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="/growth"><i class="fa fa-bar-chart-o fa-fw"></i> Анализ роста</a>
            </li>
            <li>
                <a href="/posts/"><i class="fa fa-search fa-fw"></i>Поиск постов</a>
            </li>            
            <li>
                <a href="/stats/"><i class="fa fa-table fa-fw"></i>Отслеживания</a>
            </li>
            <!--<li>
              <a href="forms.html"><i class="fa fa-edit fa-fw"></i> Граббер</a>
            </li>
            <li>
              <a href="#"><i class="fa fa-wrench fa-fw"></i> Ретаргетинг<span class="fa arrow"></span></a>
              <ul class="nav nav-second-level">
                <li>
                  <a href="panels-wells.html">Panels and Wells</a>
                </li>
                <li>
                  <a href="buttons.html">Buttons</a>
                </li>
                <li>
                  <a href="notifications.html">Notifications</a>
                </li>
                <li>
                  <a href="typography.html">Typography</a>
                </li>
                <li>
                  <a href="icons.html"> Icons</a>
                </li>
                <li>
                  <a href="grid.html">Grid</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="#"><i class="fa fa-sitemap fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
              <ul class="nav nav-second-level">
                <li>
                  <a href="#">Second Level Item</a>
                </li>
                <li>
                  <a href="#">Second Level Item</a>
                </li>
                <li>
                  <a href="#">Third Level <span class="fa arrow"></span></a>
                  <ul class="nav nav-third-level">
                    <li>
                      <a href="#">Third Level Item</a>
                    </li>
                    <li>
                      <a href="#">Third Level Item</a>
                    </li>
                    <li>
                      <a href="#">Third Level Item</a>
                    </li>
                    <li>
                      <a href="#">Third Level Item</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <a href="#"><i class="fa fa-files-o fa-fw"></i> Антиспам<span class="fa arrow"></span></a>
              <ul class="nav nav-second-level">
                <li>
                  <a href="blank.html">Blank Page</a>
                </li>
                <li>
                  <a href="login.html">Login Page</a>
                </li>
              </ul>
            </li>-->
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>