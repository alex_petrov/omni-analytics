@extends('layouts.main')
@section('body')
<?php if (isset($json)): ?>
    <script type="text/javascript">
        json = <?php echo $json; ?>;
    </script>
    <script>
        var currentPosition = 0;
    </script>    
<?php endif; ?>
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">      
            <h1 class="page-header">Поиск постов</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <?php if(isset($error)): ?>
            <div class="alert alert-success no-data">
                Неверный URL группы
            </div>               
            <?php endif; ?>
            <div class="panel-heading"  style="height:90px;">
                <form method="get" action="/posts/search" class="">
                    <span class="col-md-6">
                        URL группы в которой вы хотите найти посты:
                        <div class="form-group input-group" style="width:100%">
                            <input name="url" type="text" class="form-control">
                        </div>
                    </span>
                    <span class="col-md-6">
                        <div>&nbsp;</div>
                        <center><button type="submit" class="btn btn-default">Искать</button></center>
                    </span>
                </form>
            </div>            
            <div class="panel panel-default">
                <div class="row">
                    <div class="form-group" style="padding-left:30px;padding-top:20px;">
                        <label>Сортировка&nbsp;&nbsp&nbsp;</label>
                        <label class="radio-inline">
                            <input type="radio" name="sort" id="option-reposts" value="reposts" checked>По репостам
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="sort" id="option-likes" value="likes">По лайкам
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="sort" id="option-comments" value="comments">По комментариям
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="sort" id="option-er" value="er">По Engagement Rate
                        </label>                        
                    </div>
                </div>
                <div id="search-results" style="width: 900px;">
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

</div>
<!-- /#page-wrapper -->
@stop
@section('footer')
<!-- jQuery -->
<script src="/bower_components/jquery/dist/jquery.min.js"></script>

<script src="/js/jquery.collageplus.js"></script>
<script src="/js/readmore.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="/bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="/dist/js/sb-admin-2.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->

<script src="/js/bootstrap-datepicker.js"></script>    
<link rel="stylesheet" href="/css/datepicker.css">
<script src="/js/search.js"></script>        

@stop