@extends('layouts.main')
@section('head')
@parent
@stop
@section('body')
<script>
    var group = "<?php echo $_GET['group']; ?>";</script>
<div id="page-wrapper">
    <div class="row">

        <div class="panel-heading">
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li>
                    <a href="/growth">Группы</a>
                </li>
                <li class="active">
                    <a href="/growth/statistics?group=<?php echo $_GET['group'] ?>">Анализ</a>
                </li>
            </ul>
        </div>

        <div class="col-lg-12">
            <h1 class="page-header">Изменение количеста участников</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-success no-data" style="display:none">
                Нет данных за выбранную дату
            </div>   
            <div class="panel panel-default">
                <div class="panel-heading">
                    Вид анализа
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-pills">
                        <li class="active"><a href="#day-pills" data-toggle="tab">Данные за день</a>
                        </li>
                        <li><a href="#compare-pills" data-toggle="tab">Сравнение</a>
                        </li>
                        <li><a href="#average-pills" data-toggle="tab">Среднее за период</a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content"  style="padding-top:15px">
                        <div class="tab-pane fade in active" id="day-pills">
                            <form class="">
                                <span class="col-md-3">                        
                                    День:
                                    <div class="form-group input-group .col-md-3">
                                        <input id="date-picker" type="text" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </span>
                                <span class="col-md-3">                        
                                    Значения:
                                    <select class="form-control" id="type-picker">
                                        <option value="absolute">Абсолютные</option>
                                        <option value="relative">Относительные</option>
                                    </select>
                                </span>
                            </form>                                </div>
                        <div class="tab-pane fade" id="compare-pills">
                            <form class="">
                                <span class="col-md-3">                        
                                    Дата 1:
                                    <div class="form-group input-group .col-md-3">
                                        <input id="date-picker-1" type="text" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </span>
                                <span class="col-md-3">                        
                                    Дата 2:
                                    <div class="form-group input-group .col-md-3">
                                        <input id="date-picker-2" type="text" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </span>
                            </form>                           </div>
                        <div class="tab-pane fade" id="average-pills">
                            <form class="">
                                <span class="col-md-3">                        
                                    Начало:
                                    <div class="form-group input-group .col-md-3">
                                        <input id="date-picker-begin" type="text" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </span>
                                <span class="col-md-3">                        
                                    Конец:
                                    <div class="form-group input-group .col-md-3">
                                        <input id="date-picker-end" type="text" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </span>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>            
            <div class="panel panel-default">
                <div class="panel-heading"  style="height:90px;">
                    <form class="">
                        <span class="col-md-5">
                            Группа:
                            <h4><?php
foreach ($groups as $group):
    if ($group->domain == $_GET['group']):
        echo $group->name;
    endif;
endforeach;
?></h4>
                        </span>
                    </form>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div id="curve_chart" style="width: 900px; height: 500px"></div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

</div>
<!-- /#page-wrapper -->
@stop
@section('footer')
<!-- jQuery -->
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/js/bootstrap-datepicker.js"></script>        
<link rel="stylesheet" href="/css/datepicker.css">
<script type="text/javascript"
        src="https://www.google.com/jsapi?autoload={
        'modules':[{
        'name':'visualization',
        'version':'1',
        'packages':['corechart']
        }]
        }">
</script>
<script src="/js/chart.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="/bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="/dist/js/sb-admin-2.js"></script>
@stop