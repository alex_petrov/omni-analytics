<!DOCTYPE html>
<html lang="en">

<head>
@section('head')
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Omni Analytics 0.0.1 Beta</title>

  <link href="/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
  <link href="/dist/css/timeline.css" rel="stylesheet">
  <link href="/dist/css/sb-admin-2.css" rel="stylesheet">
  <link href="/bower_components/morrisjs/morris.css" rel="stylesheet">
  <link href="/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
@show
</head>

<body>
@section('top')
<div id="wrapper">

<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
<div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
  <a class="navbar-brand" href="/">Omni Analytics 0.0.1 Beta</a>
</div>
<!-- /.navbar-header -->
@include('modules.topnav')
<!-- /.navbar-top-links -->
@show
@section('navigation')
@include('modules.navigation')
<!-- /.navbar-static-side -->
</nav>
@show
@yield('body')
</div>
@section('footer')
<!-- /#wrapper -->

<!-- jQuery -->
<script src="/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="/bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="/bower_components/raphael/raphael-min.js"></script>
<script src="/bower_components/morrisjs/morris.min.js"></script>
<script src="/js/morris-data.js"></script>
<!-- Custom Theme JavaScript -->
<script src="/dist/js/sb-admin-2.js"></script>
@show
</body>

</html>
