<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class GetAnalyticsData extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'get.analytics.data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grabs data from vk';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        if (!Options::get("isBatchFinished")) {
            $last = Options::get("lastPosition", 0);
            $this->info("Last - " . $last);
            $groups = UserGroup::getGroupsFrom($last);
            $count = count($groups);
            $total = UserGroup::get()->count();
            foreach ($groups as $group) {
                $json = file_get_contents("https://api.vk.com/method/groups.getMembers?group_id=" . $group->domain . "&count=1");
                $data = json_decode($json);
                $growth = new Growth();
                $growth->group_id = $group->id;
                $growth->time = date("Y-m-d H:i:s", time());
                $growth->value = $data->response->count;
                $growth->save();
            }
            if($last + $count >= $total) {
                Options::set("lastPosition", 0);    
                Options::set("isBatchFinished", TRUE);
            }
            else {
                Options::set("lastPosition", $last + $count);                                
                Options::set("isBatchFinished", FALSE);                
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array();
    }

}
