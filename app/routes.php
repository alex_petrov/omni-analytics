<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@getIndex');
Route::get('login', 'LoginController@login');
Route::get('logout', 'LoginController@logout');
Route::get('groups', 'GroupsController@getIndex');
Route::get('growth/statistics', 'GrowthController@statistics');
Route::get('growth', 'GrowthController@allGroups');
Route::get('growth/add', 'GrowthController@addGroupSubmit');
Route::get('growth/delete/{domain}', 'GrowthController@removeGroupSubmit');
Route::get('stats', 'StatsController@getIndex');
Route::get('ajax/growth', 'AjaxController@growth');
Route::get('ajax/growth-compare', 'AjaxController@growthCompare');
Route::get('ajax/growth-average', 'AjaxController@growthAverage');
Route::get('grab-all', 'GrabberController@grabAll');
Route::get('publish-grabbed', 'GrabberController@publishGrabbed');
Route::get('posts', 'PostsController@getIndex');
Route::get('posts/search', 'PostsController@search');
Route::get('posts/embed/{post}', 'PostsController@embed');