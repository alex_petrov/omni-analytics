<?php

class User extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

  public $timestamps = false;

  static function current() {
      if($_SERVER['HTTP_HOST'] == 'local.omni-analytics.com') {
          return User::where('id', '=', '3559971')->first();
      }
    $cookie = Cookie::get("vk_stats_login");
    if($cookie) {
      $users = User::where('cookie_value', '=', $cookie)->get();
      $user = $users->all();
      if(!empty($user)) {
        return reset($user);
      }
      return false;
    }
    return false;
  }
  //TODO should store access token after renewal
  static function accessToken() {
    $user = User::current();
    if($user && strtotime($user->expires_at) > time()) {
      return (object) array(
        'user_id' => $user->id,
        'access_token' => $user->access_token,
        'expires_in' => $user->expires_at - time()
      );
    }
    else {
      $url = 'https://oauth.vk.com/access_token?client_id={client_id}&client_secret={secret_key}&code={code}&redirect_uri={redirect_uri}';
      $vk = Config::get('app.vk');
      extract($vk);
      $url = str_replace('{client_id}', $client_id, $url);
      $url = str_replace('{secret_key}', $client_secret, $url);
      $url = str_replace('{code}', $_GET['code'], $url);
      $url = str_replace('{redirect_uri}', $redirect_uri, $url);
      $result = file_get_contents($url);
      return json_decode($result);
    }
  }
}
