<?php

class Growth extends Eloquent {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'growth';
  public $timestamps = false;
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = array();

  static function getGrowth($domain, $date) {
    $group = UserGroup::getGroupByDomain($domain);
    $before = strtotime($date." 00:00:00");
    $after = strtotime($date." 23:59:59");
    //print $before. " " . $after; exit;
    return Growth::where('group_id', '=', $group->id)
            ->where("time", ">=", $date." 00:00:00")
            ->where("time", "<=", $date." 23:59:59")
            ->orderBy('time', 'asc')->get()->all();
  }
}
