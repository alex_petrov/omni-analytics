<?php

class Group extends Eloquent {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'groups';
  public $timestamps = false;
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = array();

  static function updateGroups($id) {
    $user = User::find($id);
    $token = User::accessToken();
    $url = "https://api.vk.com/method/groups.get?user_id={user_id}&filter=admin&extended=1&fields=members_count&access_token={access_token}";
    $url = str_replace('{user_id}', $id, $url);
    $url = str_replace('{access_token}', $token->access_token, $url);
    $json = file_get_contents($url);
    $json = json_decode($json);
    array_shift($json->response);
    $groups = $json->response;
    $ids = array();
    foreach($groups as $vkGroup) {
      $ids[] = $vkGroup->gid;
      $group = Group::find($vkGroup->gid);
      if(!$group) {
        $group = new Group();
        $group->id = $vkGroup->gid;
        $group->user_id = $id;
      }
      $group->name = $vkGroup->name;
      $group->domain = $vkGroup->screen_name;
      $group->image = $vkGroup->photo;
      $group->members = $vkGroup->members_count;
      $group->save();
    }
  }

  static function getGroups($id) {
    Group::updateGroups($id);
    return Group::where('user_id', '=', $id)->orderBy('members', 'desc')->get()->all();
  }
}
