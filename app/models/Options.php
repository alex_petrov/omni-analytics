<?php

class Options extends Eloquent {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'options';

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = array();
  public $timestamps = false;

  static public function get($name, $default = NULL) {
      $option = Options::where('name', "=", $name)->first();
      if($option) {
          return $option->value;
      }
      else {
          return $default;
      }
  }
  
  static public function set($name, $value) {
      $option = Options::where('name', "=", $name)->first();
      if(!$option) {
          $option = new Options();
          $option->name = $name;
      }
      $option->value = $value;
      $option->save();
  }
}
