<?php

class Posting extends Eloquent {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'posting';

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = array();
  public $timestamps = false;

}
