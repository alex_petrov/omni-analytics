<?php

class UserGroup extends Eloquent {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'user_groups';
  public $timestamps = false;
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = array();

  static function getGroups($id) {
    return UserGroup::all();
  }
  
  static function getGroupByDomain($domain) {
      return UserGroup::where('domain', '=', $domain)->first();
  }
  
  static function getGroupsFrom($last) {
      return UserGroup::take(100)->skip($last)->get();
  }
  
  public function url() {
      $domain = $this->domain;
      if(is_numeric($domain)) {
          $domain = "public" . $domain;
      }
      return "http://vk.com/" . $domain;
  }
}
