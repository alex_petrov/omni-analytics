<?php

class PostHash extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posthash';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array();
    public $timestamps = false;

    static function hashForPost($post) {
        $hash = PostHash::where("post", "=", $post)->first();
        if (!$hash) {
            $hash = new PostHash();
            $hash->post = $post;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://vk.com/dev.php?act=a_get_post_hash&al=1&post=" . $post);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "cookie:remixstid=1971090465_7165b1e6f64230d931; remixlang=0; remixttpid=850c5c0d27bb7419d1b363fff5f7eec65e05510abd; audio_time_left=1; remixdt=-3600; audio_vol=100; remixtst=b0270e3d; remixmdevice=360/640/3/!!!!!!!; remixmdv=tG92tQv8A793Fybd; remixrefkey=824ef6024a3a7c5408; remixsid=b30929a907f1a79f036847b6feaec253321f87ae7c7c9cdfe1c4f; remixsslsid=1; remixseenads=0; remixflash=18.0.0; remixscreen_depth=24",
                "origin:https://vk.com",
                "referer:https://vk.com/dev/Post",
                "user-agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36"
            ));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            curl_close($ch);
            $hash->hash = substr($output, strrpos($output, ">") + 1);
            $hash->date = date("Y-m-d H:i:s", time());
            $hash->save();
        }
        return $hash->hash;
    }

}
