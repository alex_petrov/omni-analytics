<?php

class Attachment extends Eloquent {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'attachments';

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = array();
  public $timestamps = false;

}
