<?php

class AjaxController extends BaseController {

    public function growth() {
        $growth = Growth::getGrowth($_GET['domain'], $_GET['date']);
        $result = array(array('Время', 'Подписчики'));

        foreach ($growth as $row) {
            $result[] = array(Date('H:i', strtotime($row->time) + 3600 * 3), intval($row->value));
        }
        for ($i = 1; $i < count($result) - 1; $i++) {
            if ($result[$i][1] == 0) {
                $result[$i][1] = ($result[$i - 1][1] + $result[$i + 1][1]) / 2;
            }
        }
        echo json_encode($result);
    }

    public function growthCompare() {
        $growth1 = Growth::getGrowth($_GET['domain'], $_GET['date1']);
        $growth2 = Growth::getGrowth($_GET['domain'], $_GET['date2']);

        $day_base1 = strtotime($_GET['date1']);
        $day_base2 = strtotime($_GET['date2']);
        $period = 900;
        $total = 60 * 60 * 24;

        $interval_start = 0;
        $result = array(array('Время', 'Дата 1', 'Дата 2'));

        while ($interval_start < $total) {
            $value1 = NULL;
            $value2 = NULL;
            foreach ($growth1 as $row) {
                if ((strtotime($row->time) >= $day_base1 + $interval_start) &&
                        (strtotime($row->time) < $day_base1 + $interval_start + $period )) {
                    $value1 = $row->value;
                    break;
                }
            }
            foreach ($growth2 as $row) {
                if ((strtotime($row->time) >= $day_base2 + $interval_start) &&
                        (strtotime($row->time) < $day_base2 + $interval_start + $period )) {
                    $value2 = $row->value;
                    break;
                }
            }
            if ($value1 && $value2) {
                $result[] = array(Date('H:i', $interval_start), $value1, $value2);
            }
            $interval_start += $period;
        }

        for ($i = 1; $i < count($result) - 1; $i++) {
            if ($result[$i][1] === 0) {
                $result[$i][1] = ($result[$i - 1][1] + $result[$i + 1][1]) / 2;
            }
            if ($result[$i][2] === 0) {
                $result[$i][2] = ($result[$i - 1][2] + $result[$i + 1][2]) / 2;
            }
        }
        echo json_encode($result);
    }

    public function growthAverage() {
        $growths = array();
        $begin = strtotime($_GET['date1']);
        $current = $begin;
        $period = 60 * 60 * 24;
        $end = strtotime($_GET['date2']);
        $count = 0;
        while ($current <= $end) {
            $growths[$current] = Growth::getGrowth($_GET['domain'], date('Y-m-d', $current));
            $current += $period;
            $count++;
        }

        $period = 900;
        $total = 60 * 60 * 24;

        $interval_start = 0;
        $result = array(array('Время', 'Подписчики'));
        while ($interval_start < $total) {
            $values = array();
            foreach ($growths as $day_base => $growth) {
                foreach ($growth as $row) {
                    if ((strtotime($row->time) >= $day_base + $interval_start) &&
                            (strtotime($row->time) < $day_base + $interval_start + $period )) {
                        $values[] = $row->value;
                        break;
                    }
                }
            }
            if (count($values) == $count) {
                $result[] = array(Date('H:i', $interval_start), $values);
            }
            $interval_start += $period;
        }
        echo json_encode($result);        
    }

}
