<?php

class PostsController extends BaseController {

    public function getIndex() {
        if (User::current()) {
            return View::make('posts');
        } else {
            return View::make('front-not-logged-in', array('link' => $this->getLink()));
        }
    }

    public function embed($post) {
        if (User::current()) {
            return View::make('embed', array('post' => $post));
        } else {
            return View::make('front-not-logged-in', array('link' => $this->getLink()));
        }
    }

    public function search() {
        if (User::current()) {
            $url = Request::input('url');
            $url = str_replace("http://vk.com/", "", $url);
            $url = str_replace("https://vk.com/", "", $url);
            $url = str_replace("http://m.vk.com/", "", $url);
            $url = str_replace("https://m.vk.com/", "", $url);
            $url = str_replace("vk.com/", "", $url);
            $url = str_replace("vk.com/", "", $url);
            $url = str_replace("m.vk.com/", "", $url);
            $url = str_replace("m.vk.com/", "", $url);
            $is_domain = TRUE;
            $id = FALSE;
            if (strpos($url, "public") === 0 || strpos($url, "club") === 0) {
                $id = str_replace(array("club", "public"), array("", ""), $url);
                if (is_numeric($id)) {
                    $is_domain = FALSE;
                }
            }
            if ($is_domain) {
                $json = file_get_contents("https://api.vk.com/method/groups.getById?group_id=" . $url . "&fields=members_count");
            } else {
                $json = file_get_contents("https://api.vk.com/method/groups.getById?group_ids=" . $id . "&fields=members_count");
            }
            $data = json_decode($json);
            if (is_object($data) && isset($data->response)) {
                $total = $data->response[0]->members_count;
            }
            $date_end = strtotime(date('Y-m-d', time())) - 3600 * 24 * 30;
            $offset = 0;
            if (is_object($data) && isset($data->response) && isset($data->response[0]->gid)) {
                do {
                    $query_string = "https://api.vk.com/method/wall.get?owner_id=-" . $data->response[0]->gid . "&count=100&filter=owner&offset=" . $offset;
                    $response = json_decode(file_get_contents($query_string));
                    $posts = $response->response;
                    if (count($posts) == 1) {
                        break;
                    }
                    $i = 0;
                    //$total = $posts[0];
                    foreach ($posts as $post) {
                        $i++;
                        if (is_object($post) && $i > 2) {
                            $attachments = array();
                            if (isset($post->attachments)) {
                                foreach ($post->attachments as $attachment) {
                                    if ($attachment->type == 'video') {
                                        $attachments[] = $attachment->video->image_small;
                                    }
                                    if ($attachment->type == 'photo') {
                                        $attachments[] = $attachment->photo->src;
                                    }
                                    if ($attachment->type == "doc") {
                                        if (isset($attachment->doc->thumb_s)) {
                                            $attachments[] = $attachment->doc->thumb_s;
                                        }
                                    }
                                }
                            }
                            $result[] = array('url' => $post->from_id . "_" . $post->id,
                                'text' => $post->text,
                                'attachments' => $attachments,
                                'likes' => $post->likes->count,
                                'reposts' => $post->reposts->count,
                                'comments' => $post->comments->count,
                                'er' => isset($total) ? ($post->likes->count + $post->reposts->count + $post->comments->count) * 100 * 5 / $total : 0);
                        }
                    }
                    $offset += 100;
                } while ($offset < 600);
                $json = json_encode($result);
                return View::make('posts', array('json' => $json));
            }
            else {
                return View::make('posts', array("error" => "wrong_group"));
            }
        } else {
            return View::make('front-not-logged-in', array('link' => $this->getLink()));
        }
    }

}
