<?php

class GrabberController extends BaseController {

  public function publishGrabbed() {
    $group = Input::get('group');
    $interval = 60;
    $startDate = Input::get('startDate');
    $startTime = Input::get('startTime');
    $time = strtotime("$startDate $startTime");
    $grabbed = GrabberPost::all();
    foreach($grabbed as $grabbedPost) {
      $post = new Post();
      $post->group_id = $group;
      $post->text = $grabbedPost->text;
      $post->date = date('Y-m-d H:i:s', $time);
      $time += 3600;
      $post->save();
      $grabbedAttachments = GrabberAttachment::where('post_id', '=', $grabbedPost->id)->get()->all();
      foreach($grabbedAttachments as $grabbedAttachment) {
        $attachment = new Attachment();
        $attachment->post_id = $post->id;
        $attachment->src = $grabbedAttachment->src;
        $attachment->save();
      }
    }
    return "Записи запланированы";
  }

  public function grabAll() {
    if(User::current()) {
      $domain = Input::get('domain');
      $endDateRaw = Input::get('end');
      $endDate = strtotime($endDateRaw);
      $offset = Input::get('offset');
      $json = file_get_contents("https://api.vk.com/method/wall.get?domain=". $domain . "&count=100&filter=owner" . ($offset ? "&offset=$offset" : ""));
      $offset += 100;
      $json = json_decode($json);
      $posts = $json->response;
      array_shift($posts);

      $group = GrabberGroup::where('domain', '=', $domain)->get()->all();
      if(empty($group)) {
        $group = new GrabberGroup();
        $group->domain = $domain;
        $group->save();
      }
      else {
        $group = reset($group);
      }
      $stop = false;
      foreach($posts as $post) {
        //Filtering out not needed content
        //skipping all video posts
        $skip = false;
        if(!isset($post->attachments) || isset($post->is_pinned)) {
          continue;
        }
        foreach($post->attachments as $attachment) {
          if("photo" !== $attachment->type) {
            $skip = true;
          }
        }

        if($post->date <= $endDate && !isset($post->is_pinned)) {
          $skip = true;
          $stop = true;
        }

        //skipping all posts with links
        $regex = "((https?|ftp)\:\/\/)?"; // SCHEME
        $regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass
        $regex .= "([a-z0-9-.]*)\.([a-z]{2,3})"; // Host or IP
        $regex .= "(\:[0-9]{2,5})?"; // Port
        $regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path
        $regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query
        $regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor

        $text = $post->text;

        if(preg_match("/^$regex$/", $text)) {
          $skip = true;
        }

        if($skip) {
          continue;
        }

        $text = str_replace("#Франция", "", $text);
        $text = str_replace("#", "", $text);

        $newPost = new GrabberPost();
        $newPost->group_id = $group->id;
        $newPost->text = trim($text);
        $newPost->save();
        foreach($post->attachments as $attachment) {
          $newAttachment = new GrabberAttachment();
          $newAttachment->post_id = $newPost->id;
          $type = $attachment->type;
          $newAttachment->type = $attachment->type;
          $sizes = array(
            "src_xxxbig",
            "src_xxbig",
            "src_xbig",
            "src_big",
            "src",
            "src_small"
          );
          for($i = 0; $i < count($sizes); $i++) {
            $size = $sizes[$i];
            if(property_exists($attachment->$type, $size)) {
              $newAttachment->src = $attachment->$type->$size;
              break;
            }
          }
          $newAttachment->save();
        }
      }
      if($stop) {
        return "Импорт завершен";
      }
      else {
        return Redirect::to("/grab-all?domain=$domain&end=$endDateRaw&offset=$offset");
      }
    }
    else {
      return Redirect::to('/');
    }
  }
}