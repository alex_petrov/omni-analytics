<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

    protected function getLink() {
        $url = "http://oauth.vk.com/authorize?client_id={client_id}scope={scope}&redirect_uri={redirect_uri}&response_type=code";
        $vk = Config::get('app.vk');
        extract($vk);
        $url = str_replace('{client_id}', $client_id, $url);
        $url = str_replace('{redirect_uri}', $redirect_uri, $url);
        $url = str_replace('{scope}', $scope, $url);
        return $url;
    }        
        
}
