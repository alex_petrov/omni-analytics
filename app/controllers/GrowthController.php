<?php

class GrowthController extends BaseController {

    public function allGroups() {
        if (User::current()) {
            return View::make('allgroups', array('groups' => UserGroup::getGroups(User::current()->id)));
        } else {
            return View::make('front-not-logged-in', array('link' => $this->getLink()));
        }
    }

    public function statistics() {
        if (User::current()) {
            return View::make('growth', array('groups' => UserGroup::getGroups(User::current()->id)));
        } else {
            return View::make('front-not-logged-in', array('link' => $this->getLink()));
        }
    }

    public function addGroupSubmit() {
        $url = Request::input('url');
        $url = str_replace("http://vk.com/", "", $url);
        $url = str_replace("https://vk.com/", "", $url);
        $url = str_replace("http://m.vk.com/", "", $url);
        $url = str_replace("https://m.vk.com/", "", $url);
        $url = str_replace("vk.com/", "", $url);
        $url = str_replace("vk.com/", "", $url);
        $url = str_replace("m.vk.com/", "", $url);
        $url = str_replace("m.vk.com/", "", $url);
        if (strpos($url, "public") !== FALSE) {
            $numeric_url = str_replace("public", "", $url);
            if (is_numeric($numeric_url)) {
                $url = "club" . $numeric_url;
            }
        }
        $exists = UserGroup::where("domain", "=", $url)->get();
        if ($exists->isEmpty()) {
            $json = file_get_contents("https://api.vk.com/method/groups.getById?group_id=" . $url);
            $data = json_decode($json);
            if (is_object($data) && isset($data->response)) {
                $exists = UserGroup::where("domain", "=", $data->response[0]->gid)->get();
                if ($exists->isEmpty()) {
                    $group = new UserGroup();
                    $group->domain = $data->response[0]->gid;
                    $group->name = $data->response[0]->name;
                    $group->owner_id = User::current()->id;
                    $group->save();
                    return Redirect::to("/growth");
                } else {
                    return Redirect::to("/growth?error=already&first");
                }
            }
        } else {
            return Redirect::to("/growth?error=already&second");
        }
        return Redirect::to("/growth?error=true");
    }

    public function removeGroupSubmit($domain) {
        if (User::current()->id == "3559971"):
            $group = UserGroup::where("domain", "=", $domain)->first();
            if ($group) {
                Growth::where("group_id", "=", $group->id)->delete();
                $group->delete();
                return Redirect::to("/growth");
            }
        endif;
        return Redirect::to("/growth?error=true");
    }

}
