<?php

class StatsController extends BaseController {

    public function getIndex() {
        if (User::current()) {
            return View::make('empty');
        } else {
            return View::make('front-not-logged-in', array('link' => $this->getLink()));
        }
    }
}
