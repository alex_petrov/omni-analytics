<?php

class LoginController extends BaseController {

  public function login()
  {
    if(array_key_exists('code', $_GET)) {
      $token = User::accessToken();
      if(is_object($token) && property_exists($token, 'access_token')) {
        $user = User::find($token->user_id);
        if(empty($user)) {
          $json = file_get_contents("https://api.vk.com/method/users.get?uids=". $token->user_id . "&fields=uid,first_name,last_name,nickname,screen_name,sex,bdate,city,country,timezone,photo&access_token=" . $token->access_token);
          $json = json_decode($json);
          $json = reset($json->response);
          $user = new User();
          $user->id = $token->user_id;
          $user->avatar = $json->photo;
          $user->name = $json->first_name . " " . $json->last_name;
        }
        if(!$user->cookie_value || strtotime($user->expires_at) < time()) {
          $user->cookie_value = "vkstats_" . md5(mt_rand());
          $user->access_token = $token->access_token;
          $user->expires_at = date('Y-m-d H:i:s', time() + $token->expires_in);
          $user->save();
          return Redirect::to('/')->withCookie(Cookie::make("vk_stats_login", $user->cookie_value , 60 * 24 * 30));
        }
      }
    }
    return Redirect::to('/');
  }

  public function logout() {
    $user = User::current();
    if($user) {
      $user->cookie_value = "";
      $user->save();
    }
    return Redirect::to('/');
  }

}
