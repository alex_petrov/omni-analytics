<?php

class GroupsController extends BaseController {

  public function getIndex()
  {
    if(User::current()) {
      return View::make('groups', array(
        'groups' => Group::getGroups(User::current()->id))
      );
    }
    else {
      return View::make('front-not-logged-in', array('link' => $this->getLink()));
    }
  }
}
