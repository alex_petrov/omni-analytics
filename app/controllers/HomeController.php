<?php

class HomeController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@showWelcome');
      |
     */

    public function getIndex() {
        if (User::current()) {
            return Redirect::to('/growth');
            //return View::make('front-logged-in');
        } else {
            return View::make('front-not-logged-in', array('link' => $this->getLink()));
        }
    }

}
